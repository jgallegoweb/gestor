<?php require 'util/load.php' ?>
<?php require 'util/usuario.php' ?>
<?php require 'comun/header.php' ?>
<?php 
    $modeloTrabajo = new ModeloTrabajo($bd);
    $trabajos = $modeloTrabajo->getList("", FALSE);
    
    $modeloLab = new ModeloExperimento($bd);
    $experimentos = $modeloLab->getList("", 1, false);
    
    $modeloCategoria = new ModeloCategoria($bd);
    $categorias = $modeloCategoria->getList("", false);
?>
<div class="container">
    <h1><a href="http://jgallegoweb.com">jgallegoweb.com</a></h1>
    <div class="row">
        <div class="col-sm-4">
            <h2>Portfolio</h2>
            <?php foreach($trabajos as $trabajo){?>
                <div class="media">
                    <div class="media-left">
                        <a href="#">
                            <img class="media-object" src="<?php echo Configuracion::RUTA ?>/images/<?php echo $trabajo->getImagen() ?>" width="70">
                        </a>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading"><?php echo $trabajo->getTitulo() ?></h4>
                        <?php echo $trabajo->getDescripcion(); ?>
                    </div>
                </div>
            <?php } ?>
            <a href="portfolio/view/index.php" class="btn btn-default btn-block">Ver portfolio completo</a>
        </div>
        <div class="col-sm-4">
            <h2>Lab</h2>
            <?php foreach($experimentos as $trabajo){?>
                <div class="media">
                    <div class="media-left">
                        <a href="#">
                            <img class="media-object" src="<?php echo Configuracion::RUTA ?>/images/<?php echo $trabajo->getImagen() ?>" width="70">
                        </a>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading"><?php echo $trabajo->getTitulo() ?></h4>
                        <?php echo $trabajo->getSubtitulo(); ?>
                    </div>
                </div>
            <?php } ?>
            <a href="lab/view/index.php" class="btn btn-default btn-block">Ver lab completo</a>
        </div>
        <div class="col-sm-4">
            <h2>Categorías</h2>
            <ul class="list-group">
            <?php foreach($categorias as $categoria){?>
                <li class="list-group-item"><?php echo $categoria->getNombre()." (".$categoria->getTipo().")" ?></li>
            <?php } ?>
            </ul>
            <a href="categoria//view/index.php" class="btn btn-default btn-block">Ver categorías</a>
        </div>
    </div>
</div>


<?php require 'comun/footer.php' ?>
