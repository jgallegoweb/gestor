<?php
require '../../util/load.php';
require_once '../../util/usuario.php';

$id = Leer::post("id");
$titulo = Leer::post("titulo");
$descripcion = Leer::post("descripcion");
$enlace = Leer::post("enlace");
$link = Leer::post("link");
$fecha = Leer::post("fecha");
$tecnologias = Leer::post("tecnologias");
$foto = $_FILES["imagen"];
$categorias = Leer::post("categorias");

$modeloTrabajo = new ModeloTrabajo($bd);
$trabajo = $modeloTrabajo->get($id);
$modeloCategoriaTrabajo = new ModeloCategoriaTrabajo($bd);
$categoriasBD = $modeloCategoriaTrabajo->getIdCategoriasByTrabajo($trabajo->getId());

//nueva foto
$fotov = "";
if($foto["size"][0]>0){
    $subir = new SubirMultiple("imagen");

    $subir->addExtension("jpg");
    $subir->addExtension("png");
    $subir->addTipo("image/jpeg");
    $subir->addTipo("image/png");
    $subir->setNuevoNombre(time());
    $subir->setAcccion(1);
    $subir->setAccionExcede(1);
    $subir->setTamanio(1024*1024*5);
    $subir->setCantidadMaxima(1);
    $subir->setCrearCarpeta(false);
    $subir->setDestino("../../../images");
    $subir->subir();
    $fotos = $subir->getNombres();
    if(isset($fotos[0])){
        $fotov = $trabajo->getImagen();
        $trabajo->setImagen($fotos[0]);
    }
}

//fecha a timestamp
if($fecha!=""){
    list($day, $month, $year) = explode('/', $fecha);
    $fecha = mktime(0, 0, 0, $month, $day, $year);
}else{
    $fecha = time();
}

//cambio de valores
$trabajo->setTitulo($titulo);
$trabajo->setDescripcion($descripcion);
$trabajo->setEnlace($enlace);
$trabajo->setLink($link);
$trabajo->setFecha($fecha);
$trabajo->setTecnologias($tecnologias);

//actualizar
$r = $modeloTrabajo->edit($trabajo);

if($r!=-1){
    if($fotov!=""){
        unlink("../../../images/".$fotov);
    }
    $borrar = [];
    foreach ($categoriasBD as $key => $categoriaBD){
        if(!in_array($categoriaBD, $categorias)){
            $modeloCategoriaTrabajo->delete($categoriaBD, $trabajo->getId());
            $borrar[] = $key;
        }
    }
    foreach ($borrar as $b){
        unset($categoriasBD[$b]);
    }
    foreach ($categorias as $categoria){
        if(!in_array($categoria, $categoriasBD)){
            $modeloCategoriaTrabajo->insert($trabajo->getId(), $categoria);
        }
    }
    Aviso::redirigir("../view/edit.php", ["men" => "Cambios realizados con exito", "a" => 1, "t" => $trabajo->getId()]);
    $bd->closeConexion();
    exit();
}
Aviso::redirigir("../view/edit.php", ["men" => "No hubo cambios", "a" => 3, "t" => $trabajo->getId()]);
$bd->closeConexion();