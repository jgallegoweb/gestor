<?php require '../../util/load.php' ?>
<?php require_once '../../util/usuario.php' ?>
<?php
    $id = Leer::get("t");
    $fecha = Leer::get("f");

    $modeloTrabajo = new ModeloTrabajo($bd);
    $trabajo = $modeloTrabajo->get($id);
    if($fecha == $trabajo->getFecha()){
        $modeloTrabajo->delete($id);
        unlink("../../../images/".$trabajo->getImagen());
        $bd->closeConexion();
        Aviso::redirigir("../view/", ["men" => "<strong>".$trabajo->getTitulo()."</strong> Se ha eliminado correctamente", "a" => 1]);
        exit();
    }
    $bd->closeConexion();
    Aviso::redirigir("../view/", ["men" => "<strong>".$trabajo->getTitulo()."</strong> No se ha podido eliminar", "a" => 4]);
