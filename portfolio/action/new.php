<?php
require '../../util/load.php';
require_once '../../util/usuario.php';

$titulo = Leer::post("titulo");
$descripcion = Leer::post("descripcion");
$enlace = Leer::post("enlace");
$link = Leer::post("link");
$fecha = Leer::post("fecha");
$tecnologias = Leer::post("tecnologias");
$foto = $_FILES["imagen"];

$categorias = Leer::post("categorias")==null ? [] : Leer::post("categorias");

$modeloTrabajo = new ModeloTrabajo($bd);
$modeloCategoriaTrabajo = new ModeloCategoriaTrabajo($bd);

$trabajo = new Trabajo();

//nueva foto
if($foto["size"][0]>0){
    $subir = new SubirMultiple("imagen");

    $subir->addExtension("jpg");
    $subir->addExtension("png");
    $subir->addTipo("image/jpeg");
    $subir->addTipo("image/png");
    $subir->setNuevoNombre(time());
    $subir->setAcccion(1);
    $subir->setAccionExcede(1);
    $subir->setTamanio(1024*1024*5);
    $subir->setCantidadMaxima(1);
    $subir->setCrearCarpeta(false);
    $subir->setDestino("../../../images");
    $subir->subir();
    $fotos = $subir->getNombres();
    if(isset($fotos[0])){
        $trabajo->setImagen($fotos[0]);
    }
}

//fecha a timestamp
if($fecha!=""){
    list($day, $month, $year) = explode('/', $fecha);
    $fecha = mktime(0, 0, 0, $month, $day, $year);
}else{
    $fecha = time();
}
//seteo de valores
$trabajo->setTitulo($titulo);
$trabajo->setDescripcion($descripcion);
$trabajo->setEnlace($enlace);
$trabajo->setLink($link);
$trabajo->setFecha($fecha);
$trabajo->setTecnologias($tecnologias);

//insertar
$r = $modeloTrabajo->insert($trabajo);

if($r!=-1){
    foreach ($categorias as $idcategoria){
        $c = $modeloCategoriaTrabajo->insert($r, $idcategoria);
    }
    Aviso::redirigir("../view/", ["men" => "<strong>".$trabajo->getTitulo()."</strong> Se ha creado correctamente", "a" => 1]);
    $bd->closeConexion();
    exit();
}
Aviso::redirigir("../view/new.php", ["men" => "<strong>".$trabajo->getTitulo()."</strong> No se ha creado correctamente.", "a" => 4]);
$bd->closeConexion();