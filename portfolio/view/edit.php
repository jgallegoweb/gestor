<?php require '../../util/load.php' ?>
<?php require_once '../../util/usuario.php'; ?>
<?php require '../../comun/header.php' ?>
<?php
    $id = Leer::get("t");
    $modeloTrabajo = new ModeloTrabajo($bd);
    $trabajo = $modeloTrabajo->get($id);
    $modeloCategoria = new ModeloCategoria($bd);
    $categorias = $modeloCategoria->getList("portfolio", false);
    $modeloCategoriaTrabajo = new ModeloCategoriaTrabajo($bd);
    $categoriastrabajo = $modeloCategoriaTrabajo->getIdCategoriasByTrabajo($trabajo->getId());
?>
<div class="container">
    <h1>Editar - <?php echo $trabajo->getTitulo() ?></h1>
    <div class="row">
        <form class="form-horizontal col-md-12" action="../action/edit.php" method="post" enctype="multipart/form-data">
            <input type="hidden" name="id" value="<?php echo $trabajo->getId() ?>">
            <h2>Trabajo</h2>
            <div class="form-group">
                <label for="in-titulo" class="col-sm-2 control-label">Titulo</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="in-titulo" name="titulo" placeholder="Titulo" value="<?php echo $trabajo->getTitulo() ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="in-descripcion" class="col-sm-2 control-label">Descripción</label>
                <div class="col-sm-10">
                    <textarea class="form-control" id="in-descripcion" name="descripcion" placeholder="Descripcion"><?php echo $trabajo->getDescripcion() ?></textarea>
                </div>
            </div>
            <div class="form-group">
                <label for="in-enlace" class="col-sm-2 control-label">Nombre enlace</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="in-enlace" name="enlace" placeholder="Descripcion" value="<?php echo $trabajo->getEnlace() ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="in-link" class="col-sm-2 control-label">Link</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="in-link" name="link" placeholder="URL" value="<?php echo $trabajo->getLink() ?>">
                </div>
            </div>

            <div class="form-group">
                <label for="date-picker-3" class="col-sm-2 control-label">Fecha</label>
                <div class="controls col-sm-10">
                    <div class="input-group">
                        <label for="date-picker-3" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span>

                        </label>
                        <input id="date-picker-3" type="text" name="fecha" class="date-picker form-control" value="<?php echo date('m/d/Y', $trabajo->getFecha()); ?>" />
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="in-tecnologias" class="col-sm-2 control-label">Tecnologías</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="in-tecnologias" name="tecnologias" placeholder="Tecnologías" value="<?php echo $trabajo->getTecnologias() ?>">
                </div>
            </div>

            <div class="form-group">
                <label for="in-foto" class="col-sm-2 control-label">Imagen</label>
                <div class="col-sm-4">
                    <img src="<?php echo Configuracion::RUTA ?>/images/<?php echo $trabajo->getImagen() ?>" class="img-responsive">
                </div>
                <div class="col-sm-6">
                    <input type="file" class="form-control" id="in-foto" name="imagen[]">
                    <p class="help-block">Imagenes de 600x341</p>
                </div>
            </div>

            <h2>Categorías</h2>
            <div class="form-group">
                <label for="in-link" class="col-sm-2 control-label">Link</label>
                <div class="col-sm-10">
                    <select name="categorias[]" multiple class="form-control">
                        <?php foreach ($categorias as $categoria) { ?>
                        <option value="<?php echo $categoria->getId() ?>" <?php if (in_array($categoria->getId(), $categoriastrabajo)){ ?>selected<?php } ?> ><?php echo $categoria->getNombre() ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-default">Guardar cambios</button>
                </div>
            </div>
        </form>
    </div>
</div>

<?php require '../../comun/footer.php' ?>