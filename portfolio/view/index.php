<?php require '../../util/load.php' ?>
<?php require_once '../../util/usuario.php' ?>

<?php require '../../comun/header.php' ?>

<?php
    $modeloTrabajo = new ModeloTrabajo($bd);
    $trabajos = $modeloTrabajo->getList("", false);
?>
<div class="container">
    <h1>Portfolio</h1>
    <div class="barra-tareas">
        <a class="btn btn-info" href="new.php">Nuevo trabajo</a>
    </div>
    
    <div class="row">
        <?php 
         foreach($trabajos as $trabajo){
        ?>
        
        <div class="col-sm-6 col-md-4 entrada">
            <div class="thumbnail">
                <img src="<?php echo Configuracion::RUTA."/images/".$trabajo->getImagen() ?>" alt="...">
                <div class="caption">
                    <h3><?php echo $trabajo->getTitulo() ?></h3>
                    <p><?php echo $trabajo->getDescripcion() ?></p>
                    <p><a type="button" class="btn btn-default" href="edit.php?t=<?php echo $trabajo->getId() ?>">Editar</a>
                        <a type="button" class="btn btn-danger boton-borrar" data-nombre="<?php echo $trabajo->getTitulo() ?>" href="../action/delete.php?t=<?php echo $trabajo->getId() ?>&f=<?php echo $trabajo->getFecha() ?>">Borrar</a></p>
                </div>
            </div>
        </div>
        <?php
         }
        ?>
    </div>
    
    <div class="modal fade" tabindex="-1" role="dialog" id="modal-borrar">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Borrar trabajo</h4>
                </div>
                <div class="modal-body">
                    <p class="modal-mensaje"></p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Cancelar</a>
                    <a href="#" class="btn btn-danger modal-boton-borrar">Borrar trabajo</a>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

</div>


<?php require '../../comun/footer.php' ?>
