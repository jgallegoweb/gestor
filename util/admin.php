<?php

$sesion->noAutentificado(Configuracion::SUBRUTA);
$idUser = $sesion->getUsuario()->getId();

$bd = new BaseDatos();
$modeloUsuario = new ModeloUsuario($bd);
$user = $modeloUsuario->get($idUser);

if($user->getRol()!="level1"){
    Aviso::redirigir(Configuracion::SUBRUTA."/", ["men" => "<strong>¡Solo admin!</strong> Solo el administrador puede acceder a esta sección.", "a" => 2]);
    exit();
}else if($user->getEstado()!="ok"){
    Aviso::redirigir(Configuracion::SUBRUTA."/user/action/logout.php", ["men" => "Tu cuenta está bloqueada.", "a" => 4]);
    exit();
}