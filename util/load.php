<?php
function autoload($clase) {
    if(file_exists('../data/clases/'.$clase . '.php')){
        require '../data/clases/'.$clase . '.php';
    }else if(file_exists('../../data/clases/'.$clase . '.php')){
        require '../../data/clases/'.$clase . '.php';
    }else if(file_exists('../../../data/clases/'.$clase . '.php')){
        require '../../../data/clases/'.$clase . '.php';
    }
}
spl_autoload_register('autoload');

$sesion = new Sesion();