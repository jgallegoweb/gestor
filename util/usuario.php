<?php

$sesion->noAutentificado(Configuracion::SUBRUTA."/user/view/login.php");
$idUser = $sesion->getUsuario()->getId();

$bd = new BaseDatos();
$modeloUsuario = new ModeloUsuario($bd);
$user = $modeloUsuario->get($idUser);
if($user->getEstado()!="ok"){
    //incluir error cuenta suspendida
    Aviso::redirigir(Configuracion::SUBRUTA."/user/action/logout.php", ["men" => "Tu cuenta está bloqueada.", "a" => 4]);
    exit();
}