$(function(){
    $(".boton-borrar").bind("click", function(e){
        e.preventDefault();
        $("#modal-borrar").modal("show");
        $(".modal-mensaje").text('¿Desea borrar "'+$(this).data("nombre")+'"?');
        $(".modal-boton-borrar").attr("href", this.href);
    });
    
    $("#new-pass").bind("click", function(e){
        $("#modal-recuperar").modal("show");
    });
});