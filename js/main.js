$(function(){
    $(".date-picker").datepicker();
    $(".date-picker").datepicker( "option", "dateFormat", "dd/mm/yy");
    $('#summernote').summernote({height: "300px"});
    $('#form-summer').on('submit', enviar);
    
    function enviar(){
        $("textarea[name='entrada']").text($('#summernote').summernote('code'));
    }
});