<?php require '../util/load.php' ?>
<?php require '../comun/headerout.php' ?>

<div class="container">
    <h1>Ayuda</h1>
    <p>Este gestor de contenidos ha sido desarrollado por <a href="http://jgallegoweb.com">jgallegoweb.com</a>.</p>
    <p>Para cualquier duda, sugerencia o asistencia técnica dirijase a <a href="http://jgallegoweb.com/contacto">jgallegoweb.com/contacto</a></p>
</div>
<?php require '../comun/footer.php' ?>