<?php require '../../util/load.php' ?>
<?php require_once '../../util/usuario.php' ?>

<?php require '../../comun/header.php' ?>

<?php
    $modeloCategoria = new ModeloCategoria($bd);
    $categorias = $modeloCategoria->getList("", false);
?>
<div class="container">
    <h1>Categoría</h1>
    <div class="barra-tareas">
        <a class="btn btn-info" href="new.php">Nueva categoría</a>
    </div>    
    <ul class="list-group">
        <?php 
         foreach($categorias as $categoria){
        ?>
        
        <li class="list-group-item">
            <a type="button" class="badge boton-borrar" data-nombre="<?php echo $categoria->getNombre()." (".$categoria->getTipo().")" ?>" href="../action/delete.php?c=<?php echo $categoria->getId() ?>&t=<?php echo $categoria->getTipo() ?>&n=<?php echo $categoria->getNombre() ?>">Borrar</a>
          <a type="button" class="badge" href="edit.php?c=<?php echo $categoria->getId() ?>">Editar</a>
          <!--span class="badge">14</span-->
          <?php echo $categoria->getNombre(); ?> (<?php echo $categoria->getTipo() ?>)
        </li>
        
        <?php
         }
        ?>
    </ul>
        
        
    <div class="modal fade" tabindex="-1" role="dialog" id="modal-borrar">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Borrar categoría</h4>
                </div>
                <div class="modal-body">
                    <p class="modal-mensaje"></p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Cancelar</a>
                    <a href="#" class="btn btn-danger modal-boton-borrar">Borrar categoría</a>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
      
    
    
</div>


<?php require '../../comun/footer.php' ?>
