<?php require '../../util/load.php' ?>
<?php require_once '../../util/usuario.php'; ?>
<?php require '../../comun/header.php' ?>
<?php
    $id = Leer::get("c");
    $modeloCategoria = new ModeloCategoria($bd);
    $categoria = $modeloCategoria->get($id);
?>
<div class="container">
    <h1>Editar categoría</h1>
    <div class="row">
        <form class="form-horizontal col-md-12" action="../action/edit.php" method="post" enctype="multipart/form-data">
            <input type="hidden" name="id" value="<?php echo $categoria->getId() ?>">
            <div class="form-group">
                <label for="in-nombre" class="col-sm-2 control-label">Nombre</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="in-nombre" name="nombre" placeholder="Nombre" value="<?php echo $categoria->getNombre() ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="in-tipo" class="col-sm-2 control-label">Tipo</label>
                <div class="col-sm-10">
                    <select class="form-control" id="in-tipo" name="tipo">
                        <option value="<?php echo $categoria->getTipo() ?>"><?php echo $categoria->getTipo() ?></option>
                        <option value="portfolio">Portfolio</option>
                        <option value="lab">Lab</option>
                    </select>
                </div>
            </div>            
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-default">Guardar</button>
                </div>
            </div>
        </form>
    </div>
</div>

<?php require '../../comun/footer.php' ?>