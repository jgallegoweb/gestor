<?php
require '../../util/load.php';
require_once '../../util/usuario.php';

$nombre = Leer::post("nombre");
$tipo = Leer::post("tipo");

$modeloCategoria = new ModeloCategoria($bd);
$categoria = new Categoria();

//seteo de valores
$categoria->setNombre($nombre);
$categoria->setTipo($tipo);

//insertar
$r = $modeloCategoria->insert($categoria);

if($r!=-1){
    Aviso::redirigir("../view/", ["men" => "<strong>".$categoria->getNombre()."</strong> Se ha creado correctamente", "a" => 1]);
    $bd->closeConexion();
    exit();
}
Aviso::redirigir("../view/new.php", ["men" => "<strong>".$categoria->getNombre()."</strong> No se ha creado correctamente.", "a" => 4]);
$bd->closeConexion();