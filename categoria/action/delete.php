<?php require '../../util/load.php' ?>
<?php require_once '../../util/usuario.php' ?>
<?php
    $id = Leer::get("c");
    $tipo = Leer::get("t");
    $nombre = Leer::get("n");

    $modeloCategoria = new ModeloCategoria($bd);
    $categoria = $modeloCategoria->get($id);
    if($tipo == $categoria->getTipo() && $nombre == $categoria->getNombre()){
        $r = $modeloCategoria->delete($id);
        if($r != -1){
            Aviso::redirigir("../view/", ["men" => "<strong>".$categoria->getNombre()."</strong> Se ha eliminado correctamente", "a" => 1]);
        }else{
            Aviso::redirigir("../view/", ["men" => "<strong>".$categoria->getNombre()."</strong> Hubo un error y no se eliminó", "a" => 4]);
        }
        $bd->closeConexion();
        exit();
    }
    $bd->closeConexion();
    Aviso::redirigir("../view/", ["men" => "<strong>".$categoria->getNombre()."</strong> No se ha podido eliminar", "a" => 4]);
