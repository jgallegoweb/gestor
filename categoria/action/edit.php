<?php
require '../../util/load.php';
require_once '../../util/usuario.php';

$id = Leer::post("id");
$nombre = Leer::post("nombre");
$tipo = Leer::post("tipo");

$modeloCategoria = new ModeloCategoria($bd);
$categoria = $modeloCategoria->get($id);

//cambio de valores
$categoria->setNombre($nombre);
$categoria->setTipo($tipo);

//actualizar
$r = $modeloCategoria->edit($categoria);

if($r!=-1){
    Aviso::redirigir("../view/edit.php", ["men" => "Cambios realizados con exito", "a" => 1, "c" => $categoria->getId()]);
    $bd->closeConexion();
    exit();
}
Aviso::redirigir("../view/edit.php", ["men" => "No hubo cambios", "a" => 3, "c" => $categoria->getId()]);
$bd->closeConexion();