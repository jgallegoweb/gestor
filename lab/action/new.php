<?php
require '../../util/load.php';
require_once '../../util/usuario.php';

$titulo = Leer::post("titulo");
$subtitulo = Leer::post("subtitulo");
$entrada = $_POST["entrada"];
$demo = Leer::post("demo");
$fecha = Leer::post("fecha");
$foto = $_FILES["imagen"];
$imagenseo = Leer::post("imagenseo");

$categorias = Leer::post("categorias")==null ? [] : Leer::post("categorias");

$modeloExperimento = new ModeloExperimento($bd);
$modeloCategoriaExperimento = new ModeloCategoriaExperimento($bd);

$experimento = new Experimento();

//nueva foto
if($foto["size"][0]>0){
    $subir = new SubirMultiple("imagen");

    $subir->addExtension("jpg");
    $subir->addExtension("png");
    $subir->addTipo("image/jpeg");
    $subir->addTipo("image/png");
    $subir->setNuevoNombre($imagenseo == "" ? time() : $imagenseo);
    $subir->setAcccion(1);
    $subir->setAccionExcede(1);
    $subir->setTamanio(1024*1024*5);
    $subir->setCantidadMaxima(1);
    $subir->setCrearCarpeta(false);
    $subir->setDestino("../../../images");
    $subir->subir();
    $fotos = $subir->getNombres();
    if(isset($fotos[0])){
        $experimento->setImagen($fotos[0]);
    }
}

//fecha a timestamp
if($fecha!=""){
    list($day, $month, $year) = explode('/', $fecha);
    $fecha = mktime(0, 0, 0, $month, $day, $year);
}else{
    $fecha = time();
}
//seteo de valores
$experimento->setTitulo($titulo);
$experimento->setSubtitulo($subtitulo);
$experimento->setEntrada($entrada);
$experimento->setDemo($demo);
$experimento->setFecha($fecha);

//insertar
$r = $modeloExperimento->insert($experimento);

if($r!=-1){
    foreach ($categorias as $idcategoria){
        $c = $modeloCategoriaExperimento->insert($r, $idcategoria);
    }
    Aviso::redirigir("../view/", ["men" => "<strong>".$experimento->getTitulo()."</strong> Se ha creado correctamente", "a" => 1]);
    $bd->closeConexion();
    exit();
}
Aviso::redirigir("../view/new.php", ["men" => "<strong>".$experimento->getTitulo()."</strong> No se ha creado correctamente.", "a" => 4]);
$bd->closeConexion();