<?php
require '../../util/load.php';
require_once '../../util/usuario.php';


$id = Leer::post("id");
$titulo = Leer::post("titulo");
$subtitulo = Leer::post("subtitulo");
$entrada = $_POST['entrada'];
$foto = $_FILES["imagen"];
$demo = Leer::post("demo");
$fecha = Leer::post("fecha");
$imagenseo = Leer::post("imagenseo");
$categorias = Leer::post("categorias");

$modeloExperimento = new ModeloExperimento($bd);
$experimento = $modeloExperimento->get($id, false);
$modeloCategoriaExperimento = new ModeloCategoriaExperimento($bd);
$categoriasBD = $modeloCategoriaExperimento->getIdCategoriasByExperimento($experimento->getId());

//nueva foto
$fotov = "";
$cx = true;
if($foto["size"][0]>0){
    $cx = false;
    $subir = new SubirMultiple("imagen");

    $subir->addExtension("jpg");
    $subir->addExtension("png");
    $subir->addTipo("image/jpeg");
    $subir->addTipo("image/png");
    $subir->setAcccion(1);
    $subir->setAccionExcede(1);
    $subir->setTamanio(1024*1024*5);
    $subir->setCantidadMaxima(1);
    $subir->setCrearCarpeta(false);
    $subir->setDestino("../../../images");
    $imagenseo = $imagenseo == "" ? time() : $imagenseo;
    $subir->setNuevoNombre($imagenseo);
    $subir->subir();
    $fotos = $subir->getNombres();
    if(isset($fotos[0])){
        $fotov = $experimento->getImagen();
        $experimento->setImagen($fotos[0]);
    }
}

if($cx && $experimento->getImagenSeo() != $imagenseo && $experimento->getImagen() != ""){
    if($imagenseo==""){
        $nuevonombre = time().".".$experimento->getImagenExt();
    }else{
        $nuevonombre = $imagenseo.".".$experimento->getImagenExt();
    }
    $x = rename("../../../images/".$experimento->getImagen(), "../../../images/".$nuevonombre);
    if($x){
        $experimento->setImagen($nuevonombre);
    }
}

//fecha a timestamp
if($fecha!=""){
    list($day, $month, $year) = explode('/', $fecha);
    $fecha = mktime(0, 0, 0, $month, $day, $year);
}else{
    $fecha = time();
}

//cambio de valores
$experimento->setTitulo($titulo);
$experimento->setSubtitulo($subtitulo);
$experimento->setEntrada($entrada);
$experimento->setFecha($fecha);
$experimento->setDemo($demo);

//actualizar
$r = $modeloExperimento->edit($experimento);

if($r!=-1){
    if($fotov!=""){
        unlink("../../../images/".$fotov);
    }
    $borrar = [];
    foreach ($categoriasBD as $key => $categoriaBD){
        if(!in_array($categoriaBD, $categorias)){
            $modeloCategoriaExperimento->delete($categoriaBD, $experimento->getId());
            $borrar[] = $key;
        }
    }
    foreach ($borrar as $b){
        unset($categoriasBD[$b]);
    }
    foreach ($categorias as $categoria){
        if(!in_array($categoria, $categoriasBD)){
            $modeloCategoriaExperimento->insert($experimento->getId(), $categoria);
        }
    }
    Aviso::redirigir("../view/edit.php", ["men" => "Cambios realizados con exito", "a" => 1, "e" => $experimento->getId()]);
    $bd->closeConexion();
    exit();
}
Aviso::redirigir("../view/edit.php", ["men" => "No hubo cambios", "a" => 3, "e" => $experimento->getId()]);
$bd->closeConexion();