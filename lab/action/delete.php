<?php require '../../util/load.php' ?>
<?php require_once '../../util/usuario.php' ?>
<?php
    $id = Leer::get("e");
    $fecha = Leer::get("f");

    $modeloExperimento = new ModeloExperimento($bd);
    $experimento = $modeloExperimento->get($id, false);
    if($fecha == $experimento->getFecha()){
        $modeloExperimento->delete($id);
        unlink("../../../images/".$experimento->getImagen());
        $bd->closeConexion();
        Aviso::redirigir("../view/", ["men" => "<strong>".$experimento->getTitulo()."</strong> Se ha eliminado correctamente", "a" => 1]);
        exit();
    }
    $bd->closeConexion();
    Aviso::redirigir("../view/", ["men" => "<strong>".$experimento->getTitulo()."</strong> No se ha podido eliminar", "a" => 4]);
