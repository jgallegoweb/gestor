<?php require '../../util/load.php' ?>
<?php require_once '../../util/usuario.php'; ?>
<?php require '../../comun/header.php' ?>
<?php
    $id = Leer::get("e");
    $modeloExperimento = new ModeloExperimento($bd);
    $experimento = $modeloExperimento->get($id, false);
    $modeloCategoria = new ModeloCategoria($bd);
    $categorias = $modeloCategoria->getList("lab", false);
    $modeloCategoriaExperimento = new ModeloCategoriaExperimento($bd);
    $categoriasexperimento = $modeloCategoriaExperimento->getIdCategoriasByExperimento($experimento->getId());
?>
<div class="container">
    <h1>Editar - <?php echo $experimento->getTitulo() ?></h1>
    <div class="row">
        <form id="form-summer" class="form-horizontal col-md-12" action="../action/edit.php" method="post" enctype="multipart/form-data">
            <input type="hidden" name="id" value="<?php echo $experimento->getId(); ?>">
            <div class="form-group">
                <label for="in-titulo" class="col-sm-2 control-label">Titulo</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="in-titulo" name="titulo" placeholder="Titulo" value="<?php echo $experimento->getTitulo() ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="in-subtitulo" class="col-sm-2 control-label">Subtítulo</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="in-subtitulo" name="subtitulo" placeholder="Subtítulo" value="<?php echo $experimento->getSubtitulo() ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="in-entrada" class="col-sm-2 control-label">Entrada</label>
                <div class="col-sm-10">
                    <textarea class="form-control hidden" id="in-entrada" name="entrada" placeholder="Entrada"></textarea>
                    <div id="summernote"><?php echo $experimento->getEntrada() ?></div>
                </div>
            </div>
            <div class="form-group">
                <label for="in-link" class="col-sm-2 control-label">Demo</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="in-link" name="link" placeholder="URL de la demo o dejar vacío" value="<?php echo $experimento->getDemo() ?>">
                </div>
            </div>

            <div class="form-group">
                <label for="date-picker-3" class="col-sm-2 control-label">Fecha</label>
                <div class="controls col-sm-10">
                    <div class="input-group">
                        <label for="date-picker-3" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span>

                        </label>
                        <input id="date-picker-3" type="text" name="fecha" class="date-picker form-control" value="<?php echo date('m/d/Y', $experimento->getFecha()); ?>" />
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="in-foto" class="col-sm-2 control-label">Imagen</label>
                <div class="col-sm-4">
                    <img src="<?php echo Configuracion::RUTA."/images/".$experimento->getImagen() ?>" class="img-responsive">
                </div>
                <div class="col-sm-6">
                    <div>
                        <label for="in-imagenseo" class="control-label">Nombre SEO de la imagen</label>
                        <div>
                            <input type="text" class="form-control" id="in-imagenseo" name="imagenseo" placeholder="ej: biotecnologia-aplicada-al-elemento o dejar vacío" value="<?php echo $experimento->getImagenSeo() ?>">
                        </div>
                    </div>
                    <div>
                        <label for="in-imagen" class="control-label">Imagen</label>
                        <div>
                            <input type="file" class="form-control" id="in-imagen" name="imagen[]">
                            <p class="help-block">Imagenes de 700px de ancho</p>
                        </div>
                    </div>
                    
                </div>
            </div>

            <h2>Categorías</h2>
            <div class="form-group">
                <label for="in-link" class="col-sm-2 control-label">Link</label>
                <div class="col-sm-10">
                    <select name="categorias[]" multiple class="form-control">
                        <?php foreach ($categorias as $categoria) { ?>
                        <option value="<?php echo $categoria->getId() ?>" <?php if (in_array($categoria->getId(), $categoriasexperimento)){ ?>selected<?php } ?> ><?php echo $categoria->getNombre() ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-default">Guardar cambios</button>
                </div>
            </div>
        </form>
    </div>
</div>

<?php require '../../comun/footer.php' ?>