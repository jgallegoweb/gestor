<?php require '../../util/load.php' ?>
<?php require_once '../../util/usuario.php'; ?>
<?php require '../../comun/header.php' ?>
<?php
    $modeloCategoria = new ModeloCategoria($bd);
    $categorias = $modeloCategoria->getList("lab", false);
    
?>
<div class="container">
    <h1>Nueva entrada</h1>
    <h2>Entrada</h2>
    <div class="row">
        <form id="form-summer" class="form-horizontal col-md-12" action="../action/new.php" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label for="in-titulo" class="col-sm-2 control-label">Titulo</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="in-titulo" name="titulo" placeholder="Titulo" value="" required>
                </div>
            </div>
            <div class="form-group">
                <label for="in-subtitulo" class="col-sm-2 control-label">Subtítulo</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="in-subtitulo" name="subtitulo" placeholder="Subtítulo" value="">
                </div>
            </div>
            <div class="form-group">
                <label for="in-entrada" class="col-sm-2 control-label">Entrada</label>
                <div class="col-sm-10">
                    <textarea class="form-control hidden" id="in-entrada" name="entrada" placeholder="Entrada"></textarea>
                    <div id="summernote"></div>
                </div>
            </div>
            <div class="form-group">
                <label for="in-link" class="col-sm-2 control-label">Demo</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="in-link" name="demo" placeholder="URL de la demo o vacío" value="">
                </div>
            </div>

            <div class="form-group">
                <label for="date-picker-3" class="col-sm-2 control-label">Fecha</label>
                <div class="controls col-sm-10">
                    <div class="input-group">
                        <label for="date-picker-3" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span>

                        </label>
                        <input id="date-picker-3" type="text" name="fecha" class="date-picker form-control" value="" />
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="in-foto" class="col-sm-2 control-label">Imagen</label>
                <div class="col-sm-10">
                    <div>
                        <label for="in-imagenseo" class="control-label">Nombre SEO de la imagen</label>
                        <div>
                            <input type="text" class="form-control" id="in-imagenseo" name="imagenseo" placeholder="ej: biotecnologia-aplicada-al-elemento" value="">
                        </div>
                    </div>
                    <div>
                        <label for="in-imagen" class="control-label">Imagen</label>
                        <div>
                            <input type="file" class="form-control" id="in-imagen" name="imagen[]" required>
                            <p class="help-block">Imagenes de 700x300</p>
                        </div>
                    </div>
                    
                </div>
            </div>
            
            <h2>Categorías</h2>
            
            <div class="form-group">
                <label for="in-link" class="col-sm-2 control-label">Link</label>
                <div class="col-sm-10">
                    <select name="categorias[]" multiple class="form-control">
                        <?php foreach ($categorias as $categoria) { ?>
                            <option value="<?php echo $categoria->getId() ?>"><?php echo $categoria->getNombre() ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>

            
            
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-default">Guardar trabajo</button>
                </div>
            </div>
        </form>
    </div>
</div>

<?php require '../../comun/footer.php' ?>