<?php require '../../util/load.php' ?>
<?php require '../../comun/headerout.php' ?>

<?php
    $sesion->siAutentificado("../../index.php");
?>
<div class="container">
    <h1>Inicio de sesión</h1>

    <form class="form-horizontal" action="../action/login.php" method="post">
        <div class="form-group">
            <label for="in-user" class="col-sm-2 control-label">Usuario</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="in-user" name="user" placeholder="Usuario">
            </div>
        </div>
        <div class="form-group">
            <label for="in-clave" class="col-sm-2 control-label">Contraseña</label>
            <div class="col-sm-10">
                <input type="password" class="form-control" id="in-clave" name="clave" placeholder="Contraseña">
            </div>
        </div>
        
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <a href="#" id="new-pass">¿Olvidaste tu contraseña?</a>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">Iniciar sesión</button>
            </div>
        </div>
    </form>

</div>
<div class="modal fade" tabindex="-1" role="dialog" id="modal-recuperar">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Recuperar contraseña</h4>
                </div>
                <form class="form-horizontal" action="../action/lost.php" method="post">
                    <div class="modal-body">
                        <p class="modal-mensaje">Por favor introduce tu email o nick. (Recuerda que si en tu cuenta no tienes un 
                            email válido no podrás recuperar la contraseña por este método. En ese caso dirigete a la sección ayuda)</p>

                            <div class="form-group">
                                <label for="in-user" class="col-sm-2 control-label">Usuario</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="in-user" name="login" placeholder="Usuario o email">
                                </div>
                            </div>

                    </div>
                    <div class="modal-footer">
                        <a href="#" class="btn btn-default" data-dismiss="modal">Cancelar</a>
                        <button type="submit" class="btn btn-info">Enviar email</button>
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

<?php require '../../comun/footer.php' ?>