<?php require '../../util/load.php' ?>
<?php
    $sesion->siAutentificado("../../index.php");
    $codigo = Leer::get("c");
    $bd = new BaseDatos();
    $modeloUsuario = new ModeloUsuario($bd);
    $usuario = $modeloUsuario->getPorCodigo($codigo);
    if($usuario->getNick() == ""){
        Aviso::redirigir(Configuracion::SUBRUTA."/user/view/login.php", ["men" => "El código no es válido o ya ha sido usado", "a" => 4]);
        $bd->closeConexion();
        exit();
    }
?>
<?php require '../../comun/headerout.php' ?>
<div class="container">
    <h1>Inicio de sesión</h1>

    <form class="form-horizontal" action="../action/found.php" method="post">
        <input type="hidden" name="codigo" value="<?php echo $codigo ?>">
        <div class="form-group">
            <label for="in-clave" class="col-sm-2 control-label">Contraseña</label>
            <div class="col-sm-10">
                <input type="password" class="form-control" id="in-clave" name="clave" placeholder="Contraseña">
            </div>
        </div>
        <div class="form-group">
            <label for="in-clave2" class="col-sm-2 control-label">Repetir contraseña</label>
            <div class="col-sm-10">
                <input type="password" class="form-control" id="in-clave2" name="clave2" placeholder="Repetir contraseña">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">Guardar nueva contraseña</button>
            </div>
        </div>
    </form>

</div>

<?php require '../../comun/footer.php' ?>