<?php require '../../util/load.php' ?>
<?php require_once '../../util/admin.php'; ?>
<?php require '../../comun/header.php' ?>
<?php
    $nick = Leer::get("u");
    $usuario = new Usuario();
    $usuario = $modeloUsuario->getPorLogin($nick);
?>
<div class="container">
    <a href="<?php echo Configuracion::SUBRUTA ?>/user/view">
        <h1>Usuarios</h1>
    </a>
    
    <div class="row">
        <div class="col-md-6">
            <h2>Datos de <?php echo $usuario->getNick() ?></h2>
            <div class="row">
                <div class="col-md-3">
                    <img src="../../../images/perfil/<?php echo $usuario->getFoto(); ?>" class="img-responsive img-thumbnail">
                </div>
                <div class="col-md-9">
                    <h3>Nick: <?php echo $usuario->getNick() ?></h3>
                    <h4>Nombre: <?php echo $usuario->getNombre();?></h4>
                    <h4>Email: <?php echo $usuario->getEmail(); ?></h4>
                </div>
                <div class="col-md-12">
                    <h5>Tipo de usuario: <?php echo $usuario->getFormatRol(); ?></h5>
                    <h5>Estado: <?php echo $usuario->getFormatEstado(); ?></h5>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <h2>Articulos escritos por <?php echo $usuario->getNick() ?></h2>
            <article>
                <h3>Articulo 1</h3>
                <p>Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto.
                    Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500,
                    cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una
                    galería de textos y los mezcló de...</p>
            </article>
            <article>
                <h3>Articulo 1</h3>
                <p>Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto.
                    Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500,
                    cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una
                    galería de textos y los mezcló de...</p>
            </article>
            <article>
                <h3>Articulo 1</h3>
                <p>Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto.
                    Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500,
                    cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una
                    galería de textos y los mezcló de...</p>
            </article>
        </div>
    </div>
</div>

<?php require '../../comun/footer.php' ?>