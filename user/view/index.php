<?php require '../../util/load.php' ?>
<?php require_once '../../util/admin.php' ?>

<?php require '../../comun/header.php' ?>

<?php $usuarios = $modeloUsuario->getList() ?>
<div class="container">
    <h1>Listado de usuarios</h1>
    <div>
        <div class="barra-tareas">
            <a class="btn btn-default" href="signin.php">Nuevo usuario</a>
        </div>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Usuario</th>
                        <th>Nombre</th>
                        <th>Email</th>
                        <th>Rol</th>
                        <th>Estado</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($usuarios as $usuario) { ?>
                    <tr>
                        <td><a href="user.php?u=<?php echo $usuario->getNick() ?>"><?php echo $usuario->getNick() ?></a></td>
                        <td><?php echo $usuario->getNombre() ?></td>
                        <td><?php echo $usuario->getEmail() ?></td>
                        <td><?php echo $usuario->getRol() ?></td>
                        <td><?php echo $usuario->getEstado() ?></td>
                        <td>
                            <a type="button" class="btn btn-default" href="edit.php?u=<?php echo $usuario->getNick() ?>">Editar</a>
                            <a type="button" 
                               class="btn <?php if($usuario->getEstado()=="ok"){ ?>btn-warning<?php }else if($usuario->getEstado()=="suspendido") { ?>btn-success<?php } ?>"
                               href="../action/block.php?u=<?php echo $usuario->getId() ?>">
                                <?php if($usuario->getEstado()=="ok"){ ?>Bloquear<?php }else if($usuario->getEstado()=="suspendido") { ?>Activar<?php } ?>
                            </a>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    
    
</div>


<?php require '../../comun/footer.php' ?>