<?php require '../../util/load.php' ?>
<?php
    $nick = Leer::get("u");
    if($nick == "me"){
        require_once '../../util/usuario.php';
        $nick = $user->getNick();
    }else{
        require_once '../../util/admin.php';
    }
?>
<?php require '../../comun/header.php' ?>
<?php
    $usuario = $modeloUsuario->getPorLogin($nick);
?>
<div class="container">
    <h1>Editar a <?php echo $usuario->getNick(); ?></h1>
    
    <div class="row">
        <form class="form-horizontal col-md-6" action="../action/edit.php" method="post" enctype="multipart/form-data">
            <h2>Editar datos</h2>
            <input type="hidden" name="id" value="<?php echo $usuario->getId() ?>">
            <div class="form-group">
                <label for="in-user" class="col-sm-2 control-label">Usuario</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="in-user" name="user" placeholder="Usuario" value="<?php echo $usuario->getNick() ?>" disabled>
                </div>
            </div>

            <div class="form-group">
                <label for="in-email" class="col-sm-2 control-label">Email</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="in-email" name="email" placeholder="Email" value="<?php echo $usuario->getEmail() ?>">
                </div>
            </div>

            <div class="form-group">
                <label for="in-nombre" class="col-sm-2 control-label">Nombre</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="in-nombre" name="nombre" placeholder="Nombre" value="<?php echo $usuario->getNombre() ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="in-rol" class="col-sm-2 control-label">Rol</label>
                <div class="col-sm-10">

                    <select class="form-control" name="rol" id="in-rol" <?php if($user->getRol()!="level1"){ echo 'disabled'; } ?>>
                        <option <?php echo $usuario->isSelectedRol("level1") ?> value="level1">Admin</option>
                        <option <?php echo $usuario->isSelectedRol("level4") ?> value="level4">Autor</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label for="in-foto" class="col-sm-2 control-label">FOTO</label>
                <div class="col-sm-10">
                    <input type="file" class="form-control" id="in-foto" name="foto[]">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-default">Guardar cambios</button>
                </div>
            </div>
        </form>
        <form class="form-horizontal col-md-6" action="../action/editpass.php" method="post" enctype="multipart/form-data">
            <h2>Editar contraseña</h2>
            <input type="hidden" name="id" value="<?php echo $usuario->getId() ?>">
            <?php if($user->getRol()!="level1"){ ?>
            <div class="form-group">
                <label for="in-clavea" class="col-sm-2 control-label">Actual</label>
                <div class="col-sm-10">
                    <input type="password" class="form-control" id="in-clavea" name="clavea" placeholder="Contraseña actual">
                </div>
            </div>
            <?php } ?>
            <div class="form-group">
                <label for="in-clave1" class="col-sm-2 control-label">Nueva</label>
                <div class="col-sm-10">
                    <input type="password" class="form-control" id="in-clave1" name="clave" placeholder="Contraseña nueva">
                </div>
            </div>
            
            <div class="form-group">
                <label for="in-clave2" class="col-sm-2 control-label">Repetir nueva</label>
                <div class="col-sm-10">
                    <input type="password" class="form-control" id="in-clave2" name="clave2" placeholder="Confirmar contraseña">
                </div>
            </div>


            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-default">Guardar cambios</button>
                </div>
            </div>
        </form>
    </div>
</div>

<?php require '../../comun/footer.php' ?>