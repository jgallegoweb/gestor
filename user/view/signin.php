<?php require '../../util/load.php' ?>
<?php require_once '../../util/admin.php' ?>
<?php require '../../comun/header.php' ?>

<div class="container">
    <h1>Nuevo usuario</h1>

    <form class="form-horizontal" action="../action/signin.php" method="post">
        <div class="form-group">
            <label for="in-user" class="col-sm-2 control-label">Usuario</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="in-user" name="user" placeholder="Usuario">
            </div>
        </div>
        <div class="form-group">
            <label for="in-clave" class="col-sm-2 control-label">Contraseña</label>
            <div class="col-sm-10">
                <input type="password" class="form-control" id="in-clave" name="clave" placeholder="Contraseña">
            </div>
        </div>
        <div class="form-group">
            <label for="in-email" class="col-sm-2 control-label">Email</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="in-email" name="email" placeholder="Email">
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">Sign in</button>
            </div>
        </div>
    </form>
    
</div>


<?php require '../../comun/footer.php' ?>