<?php
require_once '../../util/load.php';
require_once '../../util/admin.php';

$id = Leer::get("u");

$usuario = $modeloUsuario->get($id);
$tipoblock="";
if($usuario->getEstado()=="ok"){
    $usuario->setEstado("suspendido");
}else if($usuario->getEstado()=="suspendido"){
    $usuario->setEstado("ok");
    $tipoblock="des";
}
$r = $modeloUsuario->edit($usuario);
header("Location: ".Configuracion::SUBRUTA."/user/view");
if($r!=-1){
    Aviso::redirigir(Configuracion::SUBRUTA."/user/view", ["men" => "<strong>".$usuario->getNick()."</strong> ".$tipoblock."bloqueado correctamente", "a" => 1]);
}else{
    Aviso::redirigir(Configuracion::SUBRUTA."/user/view", ["men" => "<strong>".$usuario->getNick()."</strong> no se ha podido ".$tipoblock."bloquear", "a" => 4]);
}



$sesion = new Sesion();
$sesion->noAutentificado("../../index.php");
$idUser = $sesion->getUsuario()->getId();

//base de datos

$user = $modeloUsuario->get($idUser);

if($user->getRol()!="level1"){
    Aviso::redirigir(Configuracion::SUBRUTA."/user/view", ["men" => "<strong>¡Solo admin!</strong> Ya no puedes acceder a esta sección", "a" => 4]);
    exit();
}