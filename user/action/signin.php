<?php
require_once '../../util/load.php';
require_once '../../util/admin.php';

$nick = Leer::post("user");
$clave = Leer::post("clave");
$email = Leer::post("email");

if(!Validar::isLogin($nick)){
    Aviso::redirigir(Configuracion::SUBRUTA."/user/view/sigin.php", ["men" => "Nick no válido", "a" => 4]);
    $bd->closeConexion();
    exit();
}else if(!Validar::isClave($clave)){
    Aviso::redirigir(Configuracion::SUBRUTA."/user/view/signin.php", ["men" => "Clave no válida", "a" => 4]);
    $bd->closeConexion();
    exit();
}else if(!Validar::isCorreo($email)){
    Aviso::redirigir(Configuracion::SUBRUTA."/user/view/signin.php", ["men" => "Email no válido", "a" => 4]);
    $bd->closeConexion();
    exit();
}else if($modeloUsuario->getPorLogin($nick)->getNick()!=null){
    Aviso::redirigir(Configuracion::SUBRUTA."/user/view/signin.php", ["men" => "Nick en uso", "a" => 4]);
    $bd->closeConexion();
    exit();
}else if($modeloUsuario->getPorEmail($email)->getNick()!=null){
    Aviso::redirigir(Configuracion::SUBRUTA."/user/view/login.php", ["men" => "Email en uso", "a" => 4]);
    $bd->closeConexion();
    exit();
}

$usuario = new Usuario(null, $nick, Util::cifrarPass($clave), $email, $nick, "perfil.png", "level4", "", "", "ok", "none");
$idUsuario = $modeloUsuario->insert($usuario);

if($idUsuario!=-1){
    $usuario = $modeloUsuario->get($idUsuario);
    Aviso::redirigir(Configuracion::SUBRUTA."/user/view/", ["men" => "Usuario <strong>".$usuario->getNick()."</strong> creado", "a" => 1]);
    $bd->closeConexion();
    exit();
}
Aviso::redirigir(Configuracion::SUBRUTA."/user/view/login.php", ["men" => "No se ha podido crear el usuario", "a" => 4]);
$bd->closeConexion();