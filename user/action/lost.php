<?php

require_once '../../util/load.php';

$bd = new BaseDatos();
$modeloUsuario = new ModeloUsuario($bd);

$login = Leer::post("login");

if(Validar::isCorreo($login)){
    $usuario = $modeloUsuario->getPorEmail($login);
    echo "email";
}else if(Validar::isLogin($login)){
    $usuario = $modeloUsuario->getPorLogin($login);
    echo "login";
}else{
    Aviso::redirigir(Configuracion::SUBRUTA."/user/view/login.php", ["men" => "El usuario o email no existen", "a" => 3]);
    $bd->closeConexion();
    exit();
}

if($usuario->getNick()==""){
    Aviso::redirigir(Configuracion::SUBRUTA."/user/view/login.php", ["men" => "No se ha encontrado el usuario", "a" => 4]);
    $bd->closeConexion();
    exit();
}
$codigo = md5(Configuracion::PEZARANA.$usuario->getEmail().time());
$usuario->setCodigo($codigo);
$modeloUsuario->edit($usuario);
$url = Configuracion::WEB.'/gestor/user/view/find.php?c='.$codigo;

$mensaje = "<p>Para recuperar tu cuenta sigue el siguiente enlace:</p> <a href='$url'>".$url."</a>";

//enviar email
$cabeceras  = 'MIME-Version: 1.0' . "\r\n";
$cabeceras .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

// Cabeceras adicionales
$cabeceras .= 'From: Recurperación cuenta <recuperar@tucms.com>' . "\r\n";
$e = mail($usuario->getEmail(), 'Recuperación de la cuenta', $mensaje, $cabeceras);

if($e){
    Aviso::redirigir(Configuracion::SUBRUTA."/user/view/login.php", ["men" => "Hemos enviado la información de recuperación a tu email.", "a" => 1]);
}else{
    Aviso::redirigir(Configuracion::SUBRUTA."/user/view/login.php", ["men" => "Se ha producido un error. Si no recibes un email en unos minutos vuelvelo a intentar.", "a" => 1]);
}