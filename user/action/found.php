<?php

require_once '../../util/load.php';

$clave1 = Leer::post("clave");
$clave2 = Leer::post("clave2");
$codigo = Leer::post("codigo");

$bd = new BaseDatos();
$modeloUsuario = new ModeloUsuario($bd);
$usuario = $modeloUsuario->getPorCodigo($codigo);

if($usuario==null){
    Aviso::redirigir(Configuracion::SUBRUTA."/user/view/find.php", ["men" => "Enlace de recuperación no válido o ya usado. Repita el proceso.", "a" => 4]);
    $bd->closeConexion();
    exit();
}
if(Validar::isClave($clave1)){
    if($clave1 == $clave2){
        $usuario->setClave(Util::cifrarPass($clave1));
        $usuario->setCodigo("none");
        $r = $modeloUsuario->editClave($usuario);

        if($r!=-1){
            $modeloUsuario->edit($usuario);
            Aviso::redirigir(Configuracion::SUBRUTA."/user/view/login.php", ["men" => "Contraseña cambiada con éxito", "a" => 1]);
            $bd->closeConexion();
            exit();
        }
    }else{
        Aviso::redirigir(Configuracion::SUBRUTA."/user/view/find.php", ["men" => "Las contraseñas no coinciden", "a" => 4]);
        $bd->closeConexion();
        exit();
    }
}
Aviso::redirigir(Configuracion::SUBRUTA."/user/view/find.php", ["men" => "Contraseña no válida. Pruebe otra más segura", "a" => 3]);
$bd->closeConexion();
exit();