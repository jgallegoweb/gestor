<?php

require_once '../../util/load.php';
$sesion = new Sesion();

$login = Leer::post("user");
$clave = Leer::post("clave");

$bd = new BaseDatos();
$modeloUsuario = new ModeloUsuario($bd);

if(Validar::isCorreo($login)){
    $usuario = $modeloUsuario->getPorEmail($login);
}else if(Validar::isLogin($login)){
    $usuario = $modeloUsuario->getPorLogin($login);
}else{
    Aviso::redirigir(Configuracion::SUBRUTA."/user/view/login.php", ["men" => "Usuario o email no válidos", "a" => 3]);
    $bd->closeConexion();
    exit();
}
if($usuario->getId() == null){
    Aviso::redirigir(Configuracion::SUBRUTA."/user/view/login.php", ["men" => "El usuario no existe", "a" => 3]);
    $bd->closeConexion();
    exit();
}
if(Util::isPass($clave, $usuario->getClave())){
    if($usuario->getEstado()=="ok"){
        $sesion->setUsuario($usuario);
        $sesion->setAutentificado(true);
        Aviso::redirigir(Configuracion::SUBRUTA."/user/view/login.php", ["men" => "Loguin realizado", "a" => 0]);
    }else{
        Aviso::redirigir(Configuracion::SUBRUTA."/user/view/login.php", ["men" => "Tu cuenta se encuentra bloqueada. Contacta con el administrador.", "a" => 4]);
    }
    
    
    $bd->closeConexion();
    exit();
}
$sesion->destroy();
Aviso::redirigir(Configuracion::SUBRUTA."/user/view/login.php", ["men" => "Contraseña incorrecta", "a" => 3]);
$bd->closeConexion();