<?php
require_once '../../util/load.php';
require_once '../../util/usuario.php';

$id = Leer::post("id");
$regreso = "me";
if($id != $user->getId()){
    $regreso = $id;
    require_once '../../util/admin.php';
}

$clave1 = Leer::post("clave");
$clave2 = Leer::post("clave2");
$clavea = Leer::post("clavea");

$usuario = $modeloUsuario->get($id);

$clavebd = $usuario->getClave();

if(Util::isPass($clavea, $clavebd || $user->getRol == "level1")){
    if($clave1 == $clave2){
        $usuario->setClave(Util::cifrarPass($clave1));
        
        $r = $modeloUsuario->editClave($usuario);

        if($r!=-1){
            Aviso::redirigir(Configuracion::SUBRUTA."/user/view/edit.php", ["men" => "<strong>".$usuario->getNick().".</strong> Contraseña editada con éxito", "a" => 1, "u" => $regreso]);
            exit();
        }
    }else{
        Aviso::redirigir(Configuracion::SUBRUTA."/user/view/edit.php", ["men" => "Las contraseñas no coinciden", "a" => 3, "u" => $regreso]);
        exit();
    }
}
Aviso::redirigir(Configuracion::SUBRUTA."/user/view/edit.php", ["men" => "Contraseña actual incorrecta", "a" => 4, "u" => $regreso]);
exit();

