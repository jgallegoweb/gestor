<?php

require_once '../../util/load.php';
$sesion = new Sesion();
$sesion->destroy();
$mensaje = Leer::get("men");

if($mensaje == null){
    $mensaje = "Sesión cerrada correctamente";
}
Aviso::redirigir(Configuracion::SUBRUTA."/user/view/login.php", ["men" => $mensaje, "a" => 1]);

