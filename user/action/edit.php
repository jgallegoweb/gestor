<?php
require_once '../../util/load.php';
require_once '../../util/usuario.php';

$id = Leer::post("id");
$regreso = "me";
if($id != $user->getId()){
    $regreso = $id;
    require_once '../../util/admin.php';
}

$nick = Leer::post("user");
$nombre = Leer::post("nombre");
$email = Leer::post("email");
$rol = Leer::post("rol");
$foto = $_FILES["foto"];

$usuario = new Usuario();
$usuario = $modeloUsuario->get($id);

$usuario->setNombre($nombre);
$usuario->setEmail($email);
if($usuario->getRol()=="level1"){
    $usuario->setRol($rol);
}

$fotov = "";
if($foto["size"][0]>0){
    $subir = new SubirMultiple("foto");

    $subir->addExtension("jpg");
    $subir->addExtension("png");
    $subir->addTipo("image/jpeg");
    $subir->addTipo("image/png");
    $subir->setNuevoNombre(time());
    $subir->setAcccion(1);
    $subir->setAccionExcede(1);
    $subir->setTamanio(1024*1024*5);
    $subir->setCantidadMaxima(1);
    $subir->setCrearCarpeta(true);
    $subir->setDestino("../../../images/perfil");
    $subir->subir();
    $fotos = $subir->getNombres();
    if($fotos[0]!=""){
        $fotov = $usuario->getFoto();
        $usuario->setFoto($fotos[0]);
    }
}

$r = $modeloUsuario->edit($usuario);

if($r!=-1){
    if($fotov!="" && $fotov != "perfil.png"){
        unlink("../images/".$fotov);
    }
    if($id != $user->getId() || $user->getRol()=="level1"){
        Aviso::redirigir(Configuracion::SUBRUTA."/user/view/edit.php", ["men" => "<strong>".$usuario->getNick()."</strong> editado con éxito", "a" => 1, "u" => $regreso]);
    }else{
        Aviso::redirigir(Configuracion::SUBRUTA."/user/view/edit.php", ["men" => "Cambios en tu cuenta realizados con éxito", "a" => 1, "u" => $regreso]);
    }
    $bd->closeConexion();
    exit();
}
Aviso::redirigir(Configuracion::SUBRUTA."/user/view/edit.php", ["men" => "No se realizarón cambios", "a" => 4, "u" => $regreso]);
$bd->closeConexion();