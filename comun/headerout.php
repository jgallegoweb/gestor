<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Gestor</title>
        <link rel="stylesheet" type="text/css" href="<?php echo Configuracion::SUBRUTA ?>/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo Configuracion::SUBRUTA ?>/css/estilos.css">
    </head>
    <body>
        <header>
            <nav class="navbar navbar-default navbar-fixed-top">
                <div class="container">
                    <a class="navbar-brand" href="<?php echo Configuracion::SUBRUTA ?>"><img src="<?php echo Configuracion::SUBRUTA ?>/img/logo.png"></a>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="<?php echo Configuracion::SUBRUTA ?>/pages/help.php">Ayuda</a></li>
                    </ul>
                </div>
            </nav>
        </header>
        <div class="container">
            <?php Aviso::imprimir(); ?>
        </div>