<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Gestor</title>
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.css">
        <link rel="stylesheet" type="text/css" href="<?php echo Configuracion::SUBRUTA ?>/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo Configuracion::SUBRUTA ?>/css/jquery-ui.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo Configuracion::SUBRUTA ?>/css/summernote.css">
        <link rel="stylesheet" type="text/css" href="<?php echo Configuracion::SUBRUTA ?>/css/estilos.css">
    </head>
    <body>
        <header>
            <nav class="navbar navbar-default navbar-fixed-top">
                <div class="container">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="<?php echo Configuracion::SUBRUTA ?>"><img src="<?php echo Configuracion::SUBRUTA ?>/img/logo.png"></a>
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    
                    <div id="navbar" class="navbar-collapse collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Contenidos <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo Configuracion::SUBRUTA; ?>/portfolio/view">Portfolio</a></li>
                                    <li><a href="<?php echo Configuracion::SUBRUTA; ?>/lab/view">Lab</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="<?php echo Configuracion::SUBRUTA; ?>/categoria/view">Categorías</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Opciones <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo Configuracion::SUBRUTA; ?>/user/view">Usuarios</a></li>
                                    <li><a href="<?php echo Configuracion::SUBRUTA; ?>/user/view/edit.php?u=me">Mi cuenta</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="<?php echo Configuracion::SUBRUTA ?>/pages/help.php">Ayuda</a></li>
                                </ul>
                            </li>
                            <li><a href="<?php echo Configuracion::SUBRUTA ?>/user/action/logout.php">Cerrar Sesión</a></li>
                        </ul>
                    </div>
                    
                </div>
            </nav>
        </header>
        <div class="container">
            <?php Aviso::imprimir(); ?>
        </div>